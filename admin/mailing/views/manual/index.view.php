<h1>Почтовая рассылка</h1>

<? if ($cron): ?>
	последний запуск планировщика: <?=hlp::nicetime($cron) ?>
<? else: ?>
	<div class="error">Планировщик задач не работает!</div>
<? endif ?>

<table class="data_table">
<tr>
	<th>Кому</th>
	<th>Заголовок</th>
	<th>Отправлено</th>
	<th>LAST ID</th>
	<th>Всего</th>
	<th>Статус</th>
	<th></th>
</tr>
<? foreach ($tasks as $row): ?>
<tr>
	<td><?=$types_name[$row->type] ?></td>
	<td>
		<? if ($id == $row->id): ?>
			<b><?=$row->title ?></b>
		<? else: ?>
			<a href="/admin/mailing/manual/<?=$row->id ?>/"><?=$row->title ?></a>
		<? endif ?>
	</td>
	<td><?=$row->send_items ?> (<?=$row->total ? number_format(($row->send_items/$row->total)*100, 1) : 0 ?>%)</td>
	<td><?=$row->last_id ?></td>
	<td><?=$row->total ?></td>
	<td><?= $row->status ? 'Выполнено' : 'В процессе' ?></td>
	<td><a href="/admin/mailing/manual/del/<?=$row->id ?>/" onclick="return confirm('Удалить рассылку?')">Удалить</a></td>
</tr>
<? endforeach ?>
</table>


<? if ($preview): ?>
	<div style="background:#EEF; border:2px solid #DDE; margin:20px 10px; padding:10px; font:14px/16px Arial">
		<div style="border-bottom:1px solid #CCC; padding:0 0 10px; margin:0 0 10px; color:#779">Тема: <?=($title) ?></div>
		<div style="background:#FFF;">
			<?=($preview) ?>
		</div>
	</div>
<? endif ?>


<?=h_form::open() ?>
<table class="form" width="100%">
	<tr>
		<td class='label'><label>Отправить сообщение</label></td>
	</tr>
	<tr>
		<td>
		<select name="type">
			<option value="1">Всем</option>
			<option value="2">Врачам</option>
			<option value="3">Пользователям</option>
			<option value="4">Экспертам</option>
		</select>
		</td>
	</tr>
	<tr>
		<td class='field'><?=h_form::input('title', $title, 'style="width:100%"') ?></td>
	</tr>
	</tr>
	<tr>
		<td class='field'><?=h_form::textarea('message', $message, 'style="width:100%; height:200px"') ?></td>
	</tr>
</table>
<?=h_form::submit('preview', 'Предпросмотр') ?>
<?=h_form::submit('send', 'Отправить') ?>
<?=h_form::close() ?>
<br />
Тэги:<br /><br />
<b style="background:#FDD; padding:0 5px; color:#900"><code>%USERNAME%</code></b> - Имя пользователя
<br /><br />
<b style="background:#FDD; padding:0 5px; color:#900"><code>%USERMAIL%</code></b> - E-mail пользователя
<br /><br />
<b style="background:#FDD; padding:0 5px; color:#900"><code>%UNSUBSCRIBE%</code></b> - Ссылка для отписки от рассылки
<br /><br />