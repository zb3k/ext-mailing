<? if ($cron): ?>
	последний запуск планировщика: <?=hlp::nicetime($cron) ?>
<? else: ?>
	<div class="error">Планировщик задач не работает!</div>
<? endif ?>

<table class="data_table">
<tr>
	<th>#</th>
	<th>LAST UID</th>
	<th></th>
	<th>Отправлено</th>
	<th>Прочтено*</th>
	<th>Статус</th>
	<th>Период-сть (дней)</th>
	<th>Создано</th>
	<th>Последнее действие</th>
</tr>
<? foreach ($data as $i => $row): ?>
<tr class="<?=$i%2==0?'a':'b' ?>">
	<td><?=$row->id ?></td>
	<td><?=$row->last_id ?></td>
	<td><?=sys::$config->mailing->tasks[$row->type]['name'] ?></td>
	<td><?=$row->send_items ?></td>
	<td><?=$row->reading ?></td>
	<td><?=$row->status ? 'ЗАВЕРШЕНО' : 'В процессе...' ?></td>
	<td><?=$row->every ?></td>
	<td><?=hlp::date($row->postdate) ?></td>
	<td><?=hlp::date($row->lastdate) ?></td>
</tr>
<? endforeach ?>
</table>
<small>* - примерное значение</small>
<? /*

<? $data = array(
	'02.03.12-152',
	'03.03.12-12',
	'04.03.12-267',
	'05.03.12-365',
) ?>

Ожидает отправку: <b>765</b><br />

<table class="data_table">
<tr>
	<th>Дата</th>
	<th>Было отправлено</th>
	<th>Прочли</th>
</tr>
<? foreach ($data as $i => $val): ?>
<? $v = explode('-', $val) ?>
<tr class="<?=$i%2==0?'a':'b' ?>">
	<td><?=$v[0] ?></td>
	<td><?=$i==3 ? $v[1] + intval((time()-1330940639)/(400)) : $v[1] ?></td>
	<td>?</td>
</tr>
<? endforeach ?>
</table>

*/ ?>