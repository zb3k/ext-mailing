<?php



class COM_Mailing extends SYS_Component
{
	//--------------------------------------------------------------------------
	
	public $section_link = '/admin/mailing/';
	
	public $prefix = '';
	public $suffix = '';
	
	public $types = array(
		'1' => 'subscribe = 1',
		'2' => 'group_id = 2 AND subscribe = 1',
		'3' => 'group_id = 3 AND subscribe = 1',
		'4' => 'expert = 1 AND subscribe = 1',
	);
	public $types_name = array(
		'1' => 'Всем',
		'2' => 'Врачам',
		'3' => 'Пользователям',
		'4' => 'Экспертам',
	);	
	
	//--------------------------------------------------------------------------
	
	function init()
	{
		$this->load->extension('Mailing');
		$this->admin->backend_sub_menu = array(
			$this->section_link . 'auto/'   => 'Автоматическая',
			$this->section_link . 'manual/' => 'Ручная',
		);
	}
	
	//--------------------------------------------------------------------------
	
	function index()
	{
		hlp::redirect(current(array_keys($this->admin->backend_sub_menu)));
	}
	
	//--------------------------------------------------------------------------
	
	function _sub($section, $args)
	{
		$this->section_link .= $section . '/';
		
		$method    = empty($args[0]) ? 'index' : array_shift($args);
		$method_fn = $section . '_' . $method;
		
		$this->view = $section . '/' . $method;

		if (method_exists($this, $method_fn))
		{
			return call_user_func_array(array(&$this, $method_fn), $args);
		}
	}
	
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	
	function act_auto()
	{
		$args = func_get_args();
		return $this->_sub('auto', $args);
	}
	
	//--------------------------------------------------------------------------
	
	function auto_index()
	{
		$data = $this->db
			->select('mailing.*, COUNT(mailing_targets.uid) AS reading')
			->join('mailing_targets', 'mid=id')
			->group_by('mailing.id')
			->order_by('mailing.id DESC')
			->where('type!="custom" AND callback!=""')
			->get('mailing')->result();
		
		$cron = FALSE;
		if (file_exists('temp/cron.log'))
		{
			$result = file('temp/cron.log');
			$cron = intval(trim($result[0]));
		}
		
		$this->data['data'] =& $data;
		$this->data['cron'] =& $cron;
	}
	
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	

	function act_manual($id = 0)
	{
		$args = func_get_args();
		return $this->_sub('manual', $args);
	}
	
	//--------------------------------------------------------------------------
	
	function manual_del($id)
	{
		$this->db->query('DELETE FROM mailing WHERE id=? LIMIT 1', $id);
		header('Location: /admin/mailing/manual/');
		exit;
	}
	
	//--------------------------------------------------------------------------
	
	function manual_index($id = 0)
	{
//		$this->view = 'index';
		$this->data['preview'] = '';
		$this->data['title']   = '';
		$this->data['message'] = '';
		$this->data['id']      = $id;

		if (is_numeric($id) && $id)
		{
			$m = $this->db->from('mailing')->where('id=?', $id)->get()->row();
			if ($m)
			{
				$this->data['preview'] = ($m->message);
			}
		}
		
		if (! empty($_POST['message']))
		{
			$message = $_POST['message'];
			$title   = trim($_POST['title']) ? $_POST['title'] : 'Уведомление с сайта ztema.ru';
			
			$this->data['title']   = $title;
			$this->data['message'] = $message;
			
			if ( ! empty($_POST['send']))
			{
				$data = array(
					'type'    => (int)$_POST['type'],
					'message' => $this->prefix . $message . $this->suffix,
					'title'   => $title,
				);
				$this->db->insert('mailing', $data);
			}
			else
			{
				$this->load->extension('mailing');
				
				
				$this->db->where('users.id=?', $this->user->id);
				$this->data['preview'] = $this->prefix . $this->mailing->render(0, $message, $this->user->model->get_row()) . $this->suffix;
			}
		}
		else
		{
			$this->data['message'] = "Здравствуйте %USERNAME%!\n\n...\n\nВы получили это письмо, потому что зарегистрированы на сайте ztema.ru, если вы не хотите получать сообщения в дальнейшем, перейдите по <a href=\"%UNSUBSCRIBE%\">ссылке</a>.";
		}
		
		
		$tasks = $this->db->where('callback=""')->get('mailing')->result();
		
		foreach ($tasks as &$row)
		{
			$row->total = $this->db->select('COUNT(*) AS val')->where($this->types[$row->type])->get('users')->row()->val;
		}
		
		$cron = FALSE;
		if (file_exists('temp/cron.log'))
		{
			$result = file('temp/cron.log');
			$cron = intval(trim($result[0]));
		}
		

		$this->data['cron'] =& $cron;
		$this->data['tasks']      =& $tasks;
		$this->data['types_name'] =& $this->types_name;
	}
}