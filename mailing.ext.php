<?php


class EXT_Mailing
{
	//--------------------------------------------------------------------------
	
	public $send_per_process = 50;
	public $tasks = array();
	
	//--------------------------------------------------------------------------
	
	public function __construct()
	{
		sys::set_config_items(&$this, 'mailing');
		
		$this->db =& sys::$lib->db;
		
		if (CRON_MODE)
		{
			$this->cron();
		}
	}
	
	//--------------------------------------------------------------------------
	
	public function cron()
	{
		$this->check_tasks();
		$this->send_processor();
	}
	
	//--------------------------------------------------------------------------
	
	public function check_tasks()
	{
		foreach ($this->tasks as $task_key => $opt)
		{
			$time = time() - ($opt['every']*60*60*24);
			if ( ! $this->db->where('type=? AND (status=0 OR postdate>'.$time.')', $task_key)->count_all('mailing'))
			{
				$this->db->insert('mailing', array(
					'type'     => $task_key,
					'status'   => 0,
					'callback' => isset($opt['callback']) ? $opt['callback'] : '',
					'title'    => isset($opt['title']) ? $opt['title'] : '',
					'message'  => isset($opt['message']) ? $opt['message'] : '',
					'postdate' => time(),
					'every'    => isset($opt['every']) ? $opt['every'] : 0,
				));
			}
			
		}
	}
	
	//--------------------------------------------------------------------------
	
	public function render($message_id, $message, $user)
	{
		$tags = array(
			'%USERNAME%'    => $user->name ? $user->name : $user->login,
			'%USERMAIL%'    => $user->email,
			'%USERID%'      => $user->id,
			'%UNSUBSCRIBE%' => sys::$config->sys->base_url . "mailing/unsubscribe/{$user->id}/" . md5($user->email) . '/',
			'%MAILID%'      => $message_id,
		);
		
		return (str_replace(array_keys($tags), $tags, $message));
	}
	
	//--------------------------------------------------------------------------
	
	public function send_processor()
	{
//		$this->send_per_process = rand(1, $this->send_per_process);
		$fp = fopen(ROOT_PATH . "temp/mail.txt", "r+");
		
		$this->mail =& sys::$lib->load->library('mail');
		
		$status = 0;
		
		if (flock($fp, LOCK_EX | LOCK_NB))
		{
			$mail = $this->db->query('SELECT * FROM mailing WHERE status = 0 ORDER BY RAND() LIMIT 1')->row();
			
			if ( ! $mail) return;
			
			// Auto mailing
			if ($mail->callback)
			{
				for ($i=0; $i<$this->send_per_process; $i++)
				{
					if ( ! $mail) return;
					$this->_callback(&$mail);
					$mail = $this->db->query('SELECT * FROM mailing WHERE status = 0 ORDER BY RAND() LIMIT 1')->row();
				}
			}
			
			// Custom mailing
			else
			{
				$users = $this->db->query('SELECT * FROM users WHERE subscribe = 1 AND status = 1 AND id > '.intval($mail->last_id).' ORDER BY id LIMIT ' . $this->send_per_process)->result();
				
				if (count($users))
				{
					$result = 0;
					
					foreach ($users as $u)
					{
						if ( ($u->email && ! $this->mail->debug) || ($this->mail->debug && $u->email == $this->mail->debug_email) )
						{
							$message = $this->render($mail->id, $mail->message, $u);
							
							if ($message)
							{
								$this->mail->setSubject(str_replace(' ', '_', $mail->title));
								$this->mail->setHTML($message);
								$result += $this->mail->send($u->email);
							}
						}
						
						$this->set_last_uid($mail->id, $u->id);
					}
					
					$this->send_items($mail->id, $result);
				}
				else
				{
					$this->finish_mailing($mail->id);
				}
			}
			
		    ftruncate($fp, 0);
		    fwrite($fp, "Status count: {$status}\n");
		    flock($fp, LOCK_UN);
		}
		
		fclose($fp);
	}
	
	//--------------------------------------------------------------------------
	
	public function send_items($mail_id, $count = 1)
	{
		$count = intval($count);
		if ( ! $count) return;
		
		$this->db->query('UPDATE mailing SET lastdate = ?, send_items=send_items+'.$count.' WHERE id = ?', time(), $mail_id);
	}
	
	//--------------------------------------------------------------------------
	
	public function set_last_uid($mail_id, $uid)
	{
		$this->db->query('UPDATE mailing SET last_id = ?, lastdate = ? WHERE id = ?', $uid, time(), $mail_id);
	}
	
	//--------------------------------------------------------------------------
	
	public function finish_mailing($id)
	{
		$this->db->query('UPDATE mailing SET status = 1 WHERE id = ?', $id);
	}
	
	//--------------------------------------------------------------------------
	
	public function _callback(&$mail)
	{
		$args = '&$mail';
		$callback = explode('.', $mail->callback);
		switch ($callback[0])
		{
			case 'com':
				if ( ! isset(sys::$com->$callback[1])) sys::$lib->load->component($callback[1]);
				break;
				
			case 'model':
				if ( ! isset(sys::$model->$callback[1])) sys::$lib->load->model($callback[1]);
				break;
		}
		
		return eval('return sys::$' . implode('->', $callback) . '(' . $args . ');');
	}
	
	//--------------------------------------------------------------------------
}